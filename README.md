# Planung eines meetups

Planungen via Pad oder HackMD

Linkbeispiel:  https://hackmd.c3d2.de/OK-Lab-Meetup-Dresden-August-2019

## Kanäle

### meetup.com
- Zugangsdaten: Rob
- einloggen bei http://meetup.com/ als OKFN
- Zeit festlegen (Ort kann später nachgetragen werden)
- Text vom letzen mal Kopieren evtl etwas abändern

### twitter
- Zugangsdaten: Rob
- https://twitter.com/OpenDataDresden/
- Kurzer Anreisser und link zum meetup

### Mailing Liste
- all@okdd.de
- opendatadresden@lists.okfn.de
- c3d2@lists.c3d2.de
- veraltet: ~~codefordresden@lists.okfn.org~~
